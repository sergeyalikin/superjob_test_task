--
-- PostgreSQL database dump
--

-- Dumped from database version 12.0
-- Dumped by pg_dump version 12.0

-- Started on 2020-01-12 23:01:47

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2819 (class 1262 OID 40504)
-- Name: SuperJob_test_DB; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE "SuperJob_test_DB" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';


ALTER DATABASE "SuperJob_test_DB" OWNER TO postgres;

\connect "SuperJob_test_DB"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 7 (class 2615 OID 40505)
-- Name: test_task; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA test_task;


ALTER SCHEMA test_task OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 202 (class 1259 OID 40577)
-- Name: successReqs; Type: TABLE; Schema: test_task; Owner: postgres
--

CREATE TABLE test_task."successReqs" (
    guid uuid NOT NULL,
    ip character varying(128) NOT NULL,
    number integer NOT NULL,
    "fibNumber" bigint NOT NULL,
    "reqDate" timestamp with time zone NOT NULL
);


ALTER TABLE test_task."successReqs" OWNER TO postgres;

--
-- TOC entry 2813 (class 0 OID 40577)
-- Dependencies: 202
-- Data for Name: successReqs; Type: TABLE DATA; Schema: test_task; Owner: postgres
--

COPY test_task."successReqs" (guid, ip, number, "fibNumber", "reqDate") FROM stdin;
0029f90d-9c4f-4b40-b535-80cf5805c7ea	::ffff:127.0.0.1	15	610	2020-01-12 19:50:23.51+03
71e36c00-8405-4320-9d8d-a1a843686afe	::ffff:127.0.0.1	11	89	2020-01-12 19:50:26.29+03
4709118f-7176-4743-8c9d-c6bbe023461d	::ffff:127.0.0.1	7	13	2020-01-12 20:23:31.485+03
d625f8f6-40d3-493b-a61f-5b2af420b351	::ffff:127.0.0.1	3	2	2020-01-12 20:25:07.675+03
2a23e0a6-53cd-4c06-972c-712f82ce301c	::ffff:127.0.0.1	20	6765	2020-01-12 21:19:57.796+03
227bd0b3-2691-4ea0-b045-eea4057d98f2	::ffff:127.0.0.1	36	14930352	2020-01-12 22:12:50.999+03
\.


--
-- TOC entry 2686 (class 2606 OID 40581)
-- Name: successReqs successReqs_pkey; Type: CONSTRAINT; Schema: test_task; Owner: postgres
--

ALTER TABLE ONLY test_task."successReqs"
    ADD CONSTRAINT "successReqs_pkey" PRIMARY KEY (guid);


-- Completed on 2020-01-12 23:01:48

--
-- PostgreSQL database dump complete
--

