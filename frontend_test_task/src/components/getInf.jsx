import React, { Component } from 'react';
import moment from 'moment';
import 'materialize-css';

class getInf extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DBinf: [],
      offset: 0
    };
  }

  componentDidMount() {
    fetch('/getInfo', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
      body: JSON.stringify({"offset": this.state.offset})
    })
      .then((res) => res.json())
      .then((DBinf) => this.setState({ DBinf }));
  }

  offsetController = async event => {
    await this.setState({offset: this.state.offset + Number(event.target.value)})
    fetch('/getInfo', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
      body: JSON.stringify({"offset": this.state.offset})
    })
      .then((res) => res.json())
      .then((DBinf) => this.setState({ DBinf }));
    }

  render() {
    return (
      <div className="row">
        <h1>Информация о запросах</h1>
        <table>
          <tbody>
            <tr>
              <th>Номер числа</th>
              <th>Число Фибоначчи</th>
              <th>Дата и время запроса</th>
            </tr>
            {this.state.DBinf.map((elem) => (
              <tr key={elem.guid}>
                <th>{elem.number}</th>
                <th>{elem.fibNumber}</th>
                <th>{moment(elem.reqDate).format('MM-DD-YYYY:HH:mm:ss')}</th>
              </tr>
            ))}
          </tbody>
        </table>
        {(this.state.offset === 0) ? <div className="left-align col s6"><h6>Вы на первой странице</h6></div>: 
        <div className="left-align col s6">
          <button className="button1 waves-effect waves-right btn" value = "-8" onClick = {this.offsetController}>
                Предыдущая страница
          </button>
        </div>}
        {(this.state.DBinf.length < 8) ? <div className="right-align col s6"><h6>Вы на последней странице</h6></div>:
        <div className="right-align col s6">
          <button className="button1 waves-effect waves-light btn" value = "8" onClick = {this.offsetController}>
                Следующая страница
          </button>
        </div>}
      </div>
    );
  }
}

export default getInf;
