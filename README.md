# SuperJob_test_task

The following tools were selected for the development of the test task: Express as a framework for writing the test part, postgresql as a database, ORM for the database - sequelize.

To start the program locally you need to install all the necessary modules by entering the backend_test_task and frontend_test_task folders in the terminal.
```sh
npm install
```
After the installation is complete, go to superjob_test_task\backend_test_task\src\models\index.js on line 5 configure connection to your database

```sh
const sequelize = new Sequelize('{$DB_NAME}', '{$USERNAME}', '{$PASS_FROM_DB}', {
  port: {$PORT},
  dialect: '{$USED_DB}',
  schema: '{$SCHENA_NAME}',
});
```
You can use any database, but you will need to configure the connection further. You can read more about this in the sequelize [documentation](https://sequelize.org/v5/manual/getting-started.html#setting-up-a-connection) in section "Setting up a connection"

You just need to change the data for your database in these fields.


To start the server in the backend_test_task folder terminal enter the following command
```sh
node start
```
To start the user part in the console of the frontend_test_task folder enter the command
```sh
npm start
```
If you do not automatically open a window in the browser, you need to go to the 3000 port