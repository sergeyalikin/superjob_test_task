import express from 'express';
import bodyParser from 'body-parser';
import sequelize from '../models';

const urlencodedParser = bodyParser.urlencoded({
  extended: false,
});
const router = express();

router.post('/getInfo', urlencodedParser, (req, res) => {
  const offset = req.body.offset < 0 ? 0 : req.body.offset;
  sequelize.models.successReq.findAll({
    limit: 8,
    raw: true,
    offset,
    order: [
      ['reqDate', 'DESC'],
    ],
    where: {
      ip: req.ip,
    },
  }).then((DBinf) => {
    res.status(200).json(DBinf);
  });
});

export default router;
