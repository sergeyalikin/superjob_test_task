import Sequelize from 'sequelize';

require('dotenv').config();

const sequelize = new Sequelize('SuperJob_test_DB', 'postgres', process.env.DB_PASS, {
  port: 5432,
  dialect: 'postgres',
  schema: 'test_task',
});

sequelize.import('./successReq.js');

sequelize.sync({ force: false });

export default sequelize;
