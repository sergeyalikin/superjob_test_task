module.exports = (sequelize, DataTypes) => {
  const successReq = sequelize.define('successReq', {
    guid: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    ip: {
      type: DataTypes.STRING(128),
      allowNull: false,
    },
    number: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    fibNumber: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    reqDate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  }, { timestamps: false });
  return successReq;
};
