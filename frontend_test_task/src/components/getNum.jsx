import React, { Component } from 'react';
import 'materialize-css';
import '../style/getNum.css'


class GetNum extends Component {
  constructor(props){
    super(props)
    this.state = {
      fibNumber: null,
      number: "",
      result:{}
    }
  }
  
  getFibNum = () => {
    try {
      if (this.state.number === "" || !(Number(this.state.number) >= 1 && Number(this.state.number) <= 50)) {
        alert("Введите корректный номер числа Фибоначчи")
        return
      }
      fetch('/getNumber', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({"number": this.state.number})
      })
        .then(res => res.json())
        .then(result =>{
          this.setState({ result })
        })
        .then(console.log(this.state))
        .catch(error => console.log(error)
        )
        
    } catch (error) {
      alert("Невозможно выполнить запрос")
    }
  }

  numberChange = event => {
    this.setState({number: event.target.value});
  }

  render() {
    return(<div>
      <h1>Получение числа Фибоначчи</h1>
      <h5>Введите номер числа Фибоначчи</h5>
          <input className="validate" type="number" name="number" onChange={this.numberChange} min="1" max="50"/>
          <button className="btn waves-effect waves-light" onClick={this.getFibNum}>Отправить</button>
      <br/>
      <h5>Ваше число Фибоначчи: {this.state.result.fibNumber}</h5>
    </div>
    )
  };
}

export default GetNum;