import express from 'express';
import bodyParser from 'body-parser';
import fib from '../utils/getFibonacciNum';
import sequelize from '../models';

const urlencodedParser = bodyParser.urlencoded({
  extended: false,
});
const router = express();

router.post('/getNumber', urlencodedParser, (req, res) => {
  const userNumber = req.body.number;
  const result = fib(userNumber);
  const newRequest = {
    ip: req.ip,
    number: userNumber,
    fibNumber: result,
  };
  sequelize.models.successReq.create(newRequest);
  res.status(200).json({
    fibNumber: result,
    number: userNumber,
  });
});


export default router;
