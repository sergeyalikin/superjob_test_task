import React from 'react';
import {
  BrowserRouter as Router, Switch, Route, Redirect,
} from 'react-router-dom';
import 'materialize-css';
import './style/App.css';
import GetNum from './components/getNum';
import GetInf from './components/getInf';

function App() {
  return (
    <Router>
      <div className="App row">
        <div className="navbar col s3 teal accent-3 valign-wrapper">
          <ul>
            <li>
              <a href="GetInf" className="button1 waves-effect waves-light btn">
                Информация о запросах
              </a>
            </li>
            <li>
              <a href="GetNum" className="button2 waves-effect waves-light btn">
                Получить число
              </a>
            </li>
          </ul>
        </div>
        <div className="content col s9">
          <Switch>
            <Route path="/GetInf" exact>
              <GetInf />
            </Route>
            <Route path="/GetNum" exact>
              <GetNum />
            </Route>
            <Redirect to="/" />
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
