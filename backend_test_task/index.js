import express from 'express';
import bodyParser from 'body-parser';
import fibonacciNumbers from './src/routes/fibonacciNumbers';
import fibonacciInfo from './src/routes/fibonacciInfo';

const app = express();
const port = 3005;
app.use(bodyParser());

app.use(fibonacciNumbers);
app.use(fibonacciInfo);

app.listen(port);
